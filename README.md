BuildForge
==========

**BuildForge** is a set of scripts originally designed by Brian Brazil, named _builder_ and used by the gNewSense project.
The gNewSense project removed "_non-free_" things from many parts of _Ubuntu_, which serverd as a base for thier Operating System. The gNewSense project has abandoned the _builder scripts_ in favour of thier new _debderiver_.
 
**BuildForge** is based on the "builder" rev.430 scripts and reworked, modded, hacked - to fit the needs of the 
HubShark Linux Project. Debian is the _Upstream Base_ distribution. Not only are we not removing some of the stuff that gNewSense did, but we may add more to it.

 * We will remove things from the repository that we don't feel "needs to be there". This may be Debian Specific stuff or some of the CMSs, various web applications ... anything that's easily gotten somewhere online. An example of this is Drupal or Archipel - in the repo the will be old very quickly, so they're just using space.

 * Some of the scripts are very old or gNewsense specific and we may not need them now but maybe later, so for us it's considered **_"Not-4-Us"_**. The ***Not-4-Us*** folder will contain some of the original script that we are not using at this point in development due to fixes or removal.
These are in the process of being tested.

* Fixed scripts go back to there original place. 




License(s)
==========

LICENSE DETAILS:	see COPYING

GNEWSENSE BUILDER DETAILS:	see README



ISSUES, BUGS & Co.
==================

Issues, bugs, sugesstion, etc. can be (should be) places in our [ISSUES](../../issues). 

We will also keep a _MILESTONE_ setup so you can keep updated as to where devlopment is at.



Documentation
=============

As far as possible we will keep an updated [WIKI](../../../../wikis/home) on the BuildForge scripts. There may be some documentation in the _docs_ folder, but they may be somewhat old.

Getting started information is also in the [WIKI](../../../../wikis/StartHere)



----

	-- hyperclock, May 2015