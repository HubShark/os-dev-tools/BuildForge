#!/bin/bash

###############################################################################
#    (C) 2013 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.hubshark.com
#	 email:    hyperclock(at)hubshark(dot)com
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build the HubShark OS, utilizing 
#	 Debian(and/or Devuan) GNU/Linux as a base-.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see COPYING) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################
################################ INFO ##########################################
#
#	Derived from the scripts used by the gNewSense Project.
#	See the original scripts at http://gnewsense.org
#
#	Original (C) 2006 - 2009  Brian Brazil
#
#################################################################################

. config

IMGDIR=$PWD/images/

rm -rf $WORKINGDIR
mkdir -p $WORKINGDIR
cd $WORKINGDIR

apt-get source base-files$VERSION
apt-get --yes build-dep base-files$VERSION
cd base-files-*

cat > etc/motd <<EOF


The programs included with the $DIST_NAME_FULL system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

$DIST_NAME_FULL comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.
EOF

cat > etc/issue <<EOF
$DISTRONAME $BASE_RELEASE \n \l

EOF

cat > etc/issue.net <<EOF
$DISTRONAME $BASE_RELEASE
EOF

cat > etc/lsb-release <<EOF
DISTRIB_ID=$DISTRONAME
DISTRIB_RELEASE=$LIVECD_VERSION
DISTRIB_CODENAME=$BASE_RELEASE
DISTRIB_DESCRIPTION="$DIST_NAME_FULL $LIVECD_VERSION ($BASE_RELEASE)"
EOF

cat > etc/os-release <<EOF
PRETTY_NAME="$DIST_NAME_FULL $LIVECD_VERSION ($BASE_RELEASE)"
NAME="$DIST_NAME_FULL"
VERSION_ID="$LIVECD_VERSION"
VERSION="$LIVECD_VERSION ($BASE_RELEASE)"
ID=$DISTRONAME_L
ANSI_COLOR="1;31"
HOME_URL="http://www.$DOMAIN/"
SUPPORT_URL="http://support.$DOMAIN/"
BUG_REPORT_URL="http://bugs.$DOMAIN/"
EOF

# Fix expired GPG key issue (ref http://lists.gnu.org/archive/html/gnewsense-dev/2009-09/msg00027.html )
cp $IMGDIR/$DISTRONAME_L-repo-key.gpg share/
if [ `grep -c $DISTRONAME_L-repo-key.gpg debian/postinst` -eq 0 ]; then
	echo -e "\napt-key add /usr/share/base-files/$DISTRONAME_L-repo-key.gpg || true\n" >> debian/postinst
fi

# Fix changed icecat GPG key issue (ref http://lists.gnu.org/archive/html/gnewsense-users/2009-11/msg00088.html )
#cp $IMGDIR/icecat-on-launchpad.gpg share/
#if [ `grep -c "base-files/icecat-on-launchpad.gpg" debian/postinst` -eq 0 ]; then
#	echo -e "\napt-key add /usr/share/base-files/icecat-on-launchpad.gpg || true\n" >> debian/postinst
# Remove python-apt reference (First release of this patch was broken)
#	sed -i -e 'd/python-apt/' debian/postinst || true
#fi

echo | dch -D $RELEASE -v $(sed -n '1s#^.*(\(.*\)).*#\1'${DISTRONAME_L}${BASE_FILES_VERSION}'#p' debian/changelog)  "Changed to reflect $RELEASE"

dpkg-buildpackage $DPKGOPTS
